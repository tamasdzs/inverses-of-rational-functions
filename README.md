# Inverses of Rational Functions

A collection of demonstrative MatLab scripts, which showcase recently developed numerical methods to produce the inverses of Blaschke-products and rational functions along a curve.

Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
Technology from the source of the National Research, Development and Innovation Fund.
