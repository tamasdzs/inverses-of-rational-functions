% Find inverse branches of n-factor Blaschke-products using the developed
% algorithm, locally employing Newton's method

% Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
% Technology from the source of the National Research, Development and Innovation Fund.

addpath('../Common');

%% Clear everything just to be sure
clear all;
close all;

%% Constants
a = [-0.11 + 1i*0.15, ...
      0.1 + 1i*0.73, ...
      -0.37 - 1i*0.2];
  
u = exp(1i*pi/2);

fi = linspace(0, 2*pi, 1024);
T  = exp(1i*fi);

% Normalize Blaschke-products
e = blaschke_vals(a, 1);

% Critical points
cp = findCriticalPoints(a);
cp = cp(abs(cp) < 1);
cp = sort(cp);

cp_ims = zeros(size(cp));
for k=1:length(cp)
   cp_ims(k) = blaschke_vals(a, cp(k)); 
end
cp_ims = cp_ims./e;

u = exp(1i*120);

%% Verify inverse values on the Torus

% Now use the interval halving method relying on the formula
T_inv = zeros(size(a));
for k=0:length(a)-1
    beta_inv = Beta_inv(a, angle(u)/length(a) + 2*k*pi/length(a));
    T_inv(k+1) = exp(1i*beta_inv);    
end

% plot the results
figure(1);
subplot(121); hold on;
plot(T, 'k', 'LineWidth', 2);
plot(0,0, 'ro', 'MarkerSize', 8, 'MarkerFaceColor', 'r');
plot(real(cp_ims), imag(cp_ims), 'co', ...
    'MarkerSize', 8, 'MarkerFaceColor', 'c');
plot(real(u), imag(u), 'bo', 'MarkerSize', 8, 'MarkerFaceColor', 'b');
title('$B(D)$', 'interpreter', 'latex');
grid on;
axis equal;

subplot(122); hold on;
plot(T, 'k', 'LineWidth', 2);
plot(T_inv, 'bo', 'MarkerSize', 8, 'MarkerFaceColor', 'b');
plot(a, 'ro', 'MarkerSize', 8, 'MarkerFaceColor', 'r');
plot(cp, 'co', 'MarkerSize', 8, 'MarkerFaceColor', 'c');
title('$D$', 'interpreter', 'latex');
grid on;
axis equal;

% Now use the local inverse lemma to find the poles
step = 0.001;
rho = 1 - step;
phi = angle(u);
z_0 = transpose(T_inv);
z_0_old = z_0;

% Callbacks
f = @(z) blaschke_vals(a, z)./e;
f_der = @(z) blaschke_derivative_ver_2(a, z)/e;

% Holder for verification values
ws = [];
w = u;

iter = 0;
max_iter = 1000;

% Solve with Newton's method
while abs(w) > 0.001 && iter < max_iter
   iter = iter+1;
   w = rho*exp(1i*angle(u));
   ws = [ws, w];
   
   z_0_old = [z_0_old, invert_w_Newton(w, z_0_old(:,end), f, f_der, eps)];
   
   subplot(121);
   plot(w, 'go','MarkerSize', 8, 'MarkerFaceColor', 'g'); hold on;
   subplot(122);
   plot(z_0_old(:, end), 'go', 'MarkerSize', 10, 'MarkerFaceColor', 'g'); hold on;
   pause(0.07);
   
   % update rho
   rho = rho-step;
end
