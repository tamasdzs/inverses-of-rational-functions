%DESCRIPTION: 
% Calculate values of a transfer function using input-output pairs and look
% at the error of the result. The input series will consist of weighted
% geometric series in order to keep the values below a given threshold.

% Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
% Technology from the source of the National Research, Development and Innovation Fund.

%% Clear & Close everything
clear all;
close all;

%% Add helper functions
addpath('../Common');

%% Visual controls
show = true;

%% Define transfer function, and series specifics
a = [0.1 + 1i*0.0, 0.2 - 1i*0.0, 0.3 + 1i*0.0]; % Zeros of the transfer function
b = [1.3 - 1i*0, 2.0 + 1i*0]; % Poles of the transfer function

% Callback to the transfer function to check approximation errors
f = @(z)  polyval(poly(a), z)/polyval(poly(b), z);

% Number of data points in the input/output series
N = 125;

% Center of the geometric series. We approximate the value of the transfer
% function at 1/c. The number c has to fulfill the following criterion for
% convergence: abs(1/c) < min(abs(b))
c = 4 + 1i*2;

%% Generate input series
[u, c0] = generate_weighted_geometric_input(c, 100000, N);

%% Generate output of SISO
y = siso_from_zeros_and_poles(u, a, b);

%% Using the output, approximate the value of the transfer function at 1/c
apr = y./u;
transf_val = f(1/c);  
err = abs(transf_val - apr);

%% Display input/output signals
if show
    % Display actual transfer function value, approximation using input/output
    % pairs and the difference between the two.
    disp(['f(1/c)', ' ; ', 'y(N)/u(N)', ' ; ', '|f(1/c) - y(N)/u(N)|:']);
    disp(mat2str([transf_val, apr(end), err(end)]));
    
    figure();
    subplot(311);
    plot(abs(u(end-20:end)), 'go', 'LineWidth', 2);
    grid on;
    title('Absolute value of weighted geometric input');
    subplot(312);
    plot(abs(y(end-20:end)),'bo', 'LineWidth', 2);
    grid on;
    title('Absolute value of SISO output');
    subplot(313);
    plot(err, 'ro', 'LineWidth', 2);
    grid on;
    title('abs(y(k)/u(k) - f(1/c))');
end