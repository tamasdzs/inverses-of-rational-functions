%% DEMO Software for lense creation through inverting n-factor Blaschke-products

% Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
% Technology from the source of the National Research, Development and Innovation Fund.

classdef light_paths < matlab.apps.AppBase
    
    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                      matlab.ui.Figure
        CurrentstateLabel             matlab.ui.control.Label
        StateLabel                    matlab.ui.control.Label
        StateselectionLabel           matlab.ui.control.Label
        RegisterZerosButton           matlab.ui.control.Button
        RegistercurveendpointsButton  matlab.ui.control.Button
        RunButton                     matlab.ui.control.Button
        StopButton                    matlab.ui.control.Button
        CurvetypeButtonGroup          matlab.ui.container.ButtonGroup
        EuclideanlinesegmentButton    matlab.ui.control.RadioButton
        HyperboliclinesegmentButton   matlab.ui.control.RadioButton
        UIAxes                        matlab.ui.control.UIAxes
        UIAxes_2                      matlab.ui.control.UIAxes
        CurrentState
        ZerosOfB
        EndpointsOfG
        CurveType
        B
        Gamma
        Abort
    end

    % Component initialization
    methods (Access = private)
        
        % Refractive index original
        function u = refractiveIndHyp(app, z)
            u = 1 - abs(z).^2;
        end
        
        % Refractive index for Blaschkes
        function u = refractiveIndBlasch(app, z, normalising_factor)
            u = (1-abs(app.B.values(z)).^2) ...
                      ./ abs(blaschke_derivative_ver_2(app.ZerosOfB, z, normalising_factor));
        end
        
        % Clear axes
        function clearAxes(app)
           cla(app.UIAxes);
           cla(app.UIAxes_2);
        end
        
        % DrawTorus3D
        function drawTorus3D(app, normalising_factor, X, Y, refr_or, refr_B, cp)
           % Draw the 3D toruses
           fi = linspace(0, 2*pi, 256);
           T = exp(1i.*fi);
           plot3(app.UIAxes, real(T), imag(T), refractiveIndHyp(app, T), 'k', 'LineWidth', 2);
           plot3(app.UIAxes_2, real(T), imag(T), refractiveIndBlasch(app, T, normalising_factor), 'k', 'LineWidth', 2);
           
           % Draw 3D curve and origin
           plot3(app.UIAxes, 0, 0, refractiveIndHyp(app, 0+1i*0), 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
           
           for k=1:size(app.Gamma,1)
            plot3(app.UIAxes, real(app.Gamma(k,:)), imag(app.Gamma(k,:)), refractiveIndHyp(app, app.Gamma(k,:)), 'b', 'LineWidth', 2);
           end
           
           refr_B(refr_B > 2) = 2;
           
           % Draw zeros of B
           plot3(app.UIAxes_2, real(app.ZerosOfB), imag(app.ZerosOfB), refractiveIndBlasch(app, app.ZerosOfB, normalising_factor), ...
               'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
          
           idx = find(imregionalmax(refr_B));
           
           CP_peaks = sort(refr_B(idx), 'descend');
           B_vals = CP_peaks(1:length(cp));
           
           idx = [];
           for k=1:length(B_vals)
              idx = [idx, find(refr_B == ones(size(refr_B))*B_vals(k))]; 
           end
           
           cp = sort(cp, 'descend');
           
           % Draw surfaces
           surf(app.UIAxes, X, Y, refr_or, 'LineStyle', 'none');
           surf(app.UIAxes_2, X, Y, refr_B, 'LineStyle', 'none');
           
           % Draw critical points
           plot3(app.UIAxes_2, real(cp), imag(cp), B_vals, ...
                 'co', 'MarkerSize', 10, 'MarkerFaceColor', 'c');
        end
        
        % DrawTorus2D
        function drawTorus2D(app)
           fi = linspace(0,2*pi,1024);
           T = exp(1i*fi);
            
           plot(app.UIAxes, T, 'k', 'LineWidth', 2);
           plot(app.UIAxes_2, T, 'k', 'LineWidth', 2);
        end
        
        % Register a zero for the Blaschke-product
        function registerBlaschkeZero(app)
           currentPoint = app.UIAxes_2.CurrentPoint(1, 1:2);
           app.ZerosOfB = [app.ZerosOfB, ...
                            currentPoint(1) + 1i*currentPoint(2)];
           
           plot(app.UIAxes_2, currentPoint(1) + 1i*currentPoint(2), ...
               'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
        end
        
        function registerCurve(app)
           
           if app.EuclideanlinesegmentButton.Value ~= true
               currentPoint = app.UIAxes.CurrentPoint(1, 1:2);
               if length(app.EndpointsOfG) < 2
                  app.EndpointsOfG = [app.EndpointsOfG, ...
                      currentPoint(1) + 1i*currentPoint(2)];

                  plot(app.UIAxes, currentPoint(1) + 1i*currentPoint(2), ...
                    'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b');
               else
                   drawCurve(app);
               end
           else
              N = 256;
              M = 16;
              phi = linspace(0, pi, M);
              t = linspace(-1, 1, N);
              app.Gamma = zeros(M, N);
              
              for k=1:M
                  for j=1:N
                    app.Gamma(k,j) = t(j)*exp(1i*phi(k));
                  end
              end
              
              drawRays(app, M);
           end
        end
        
        function drawRays(app, N)
            for k=1:N
               plot(app.UIAxes, real(app.Gamma(k,:)), imag(app.Gamma(k,:)), 'b');
               plot(app.UIAxes, 0, 0, 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
            end
        end
        
        function drawCurve(app)
            app.Gamma = hyp_line(app.EndpointsOfG(1), app.EndpointsOfG(2), 512);
            plot(app.UIAxes, app.Gamma, 'b', 'LineWidth', 2);
            plot(app.UIAxes, 0, 0, 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
        end
        
        % Handle mouse actions according to the current state
        function UIFigureWindowButtonDown(app, event)
            switch app.CurrentState
                case STATE.REG_ZER
                    registerBlaschkeZero(app);
                case STATE.REG_CURVE
                    registerCurve(app);
                otherwise
                    % left empty intentionally
            end
        end
        
        % State setter
        function succ = setState(app, newState, newText)
            
            succ = 1;
            
            switch newState
                case STATE.IDLE
                    app.CurrentState = newState;
                    app.StateLabel.Text = newText;
                case STATE.REG_ZER
                    if any([app.CurrentState == STATE.IDLE, ...
                            app.CurrentState == STATE.REG_CURVE])
                        app.CurrentState = newState;
                        app.StateLabel.Text = newText;
                    else
                        errorMsg = sprintf('Bad state change to register zeros\n');
                        msgbox(errorMsg);
                        succ = 0;
                    end
                case STATE.REG_CURVE
                    if app.CurrentState == STATE.IDLE || ...
                            app.CurrentState == STATE.REG_ZER
                        app.CurrentState = newState;
                        app.StateLabel.Text = newText;
                    else
                        errorMsg = sprintf('Bad state change to register curve\n');
                        msgbox(errorMsg);
                        succ = 0;
                    end
                case STATE.RUNNING
                    if any([app.CurrentState == STATE.REG_CURVE, ...
                            app.CurrentState == STATE.REG_ZER])
                        app.CurrentState = newState;
                        app.StateLabel.Text = newText;
                    else
                        errorMsg = sprintf('Bad state change to running\n');
                        msgbox(errorMsg);
                        succ = 0;
                    end
                otherwise
                    errorMsg = sprintf('Unavailable state requested\n');
                    msgbox(errorMsg);
                end
        end
        
        function changeAxesView(app, dim)
           switch dim
               case 2
                    view(app.UIAxes, 2);
                    view(app.UIAxes_2, 2);
               case 3
                    view(app.UIAxes, [5, 2, 5]);
                    view(app.UIAxes_2, [5, 2, 5]);
               otherwise
                    errorMsg = sprintf('Unavailable view requested\n');
                    msgbox(errorMsg);
           end
        end
        
        % Button pushed function: Button
        function RegisterZerosButtonPushed(app, event)
            setState(app, STATE.REG_ZER, 'registering zeroes of Blaschke-product');
        end
        
        function RegistercurveendpointsButtonPushed(app, event)
            setState(app, STATE.REG_CURVE, 'registering endpoints of curve');
        end
        
        function RunButtonPushed(app, event)
            setState(app, STATE.RUNNING, 'running...');
            
            % Disable abort
            app.Abort = false;
            
            % Construct Blaschke-function
            app.B = blaschkep(app.ZerosOfB);
            normalising_factor = app.B.values(1);
            app.B = blaschkep(app.ZerosOfB, 1/normalising_factor);
                            
            [T, R] = meshgrid(linspace(0, 2*pi, 256), linspace(0, 0.99, 256));
            X = R.*cos(T);
            Y = R.*sin(T);
            
            Z_normal = refractiveIndHyp(app, X + 1i.*Y);
            Z_blasch = refractiveIndBlasch(app, X + 1i.*Y, normalising_factor);
            
            % Get critical points
            cp = findCriticalPoints(app.ZerosOfB);
            cp = cp(abs(cp)<1);
            
            % Get Inverse Images of Gamma
            
            % Clear axes
            clearAxes(app);
            
            % Change axes views to 3D
            changeAxesView(app, 3);
            
            % plot 3D unit circles
            drawTorus3D(app, normalising_factor, X, Y, Z_normal, Z_blasch, cp);
            
            % Plot Blaschke-image of critical points
            plot3(app.UIAxes, real(app.B.values(cp)), ...
                    imag(app.B.values(cp)), 1 - abs(app.B.values(cp)).^2, ...
                    'co', 'MarkerSize', 10, 'MarkerFaceColor', 'c');
                
            % pause for explonation
            %pause();
            
            % Find borders on normal lens
            borders_on_normal_lens = findBorders(app.ZerosOfB, ...
                                                 cp, ...
                                                 512, ...
                                                 normalising_factor);
            
            % Plot borders
            for k=1:size(borders_on_normal_lens, 1)
               plot3(app.UIAxes, real(borders_on_normal_lens(k,:)), ...
                   imag(borders_on_normal_lens(k,:)), ...
                   1 - abs(borders_on_normal_lens(k,:)).^2, 'r', ...
                   'LineWidth', 4);
               
               inv_borders = findInvImages(app.ZerosOfB, ...
                    borders_on_normal_lens(k,:), ...
                    normalising_factor);
               
               for s=1:size(inv_borders,1)
                   
                   curr_diff = Inf;
                   for m=1:length(cp)
                       if min(abs(inv_borders(s,:)-cp(m))) < curr_diff
                           curr_diff = min(abs(inv_borders(s,:)-cp(m)));
                       end
                   end
                   
                   if curr_diff < 0.01
                       inv_borders_h = refractiveIndBlasch(app,inv_borders(s,:), normalising_factor);
                       inv_borders_h(inv_borders_h>2) = 2;
                       %inv_borders_h = inv_borders_h./max(max(inv_borders_h));

                       plot3(app.UIAxes_2, real(inv_borders(s,:)), ...
                       imag(inv_borders(s,:)), ...
                       inv_borders_h , 'r', ...
                       'LineWidth', 4);
                   end
               end
               
            end
            
            % Finally show the inverse image of Q
            for m=1:size(app.Gamma, 1)
                Q_inv = findInvImages(app.ZerosOfB, app.Gamma(m,:), normalising_factor);
                
                if size(app.Gamma, 1) > 1
                    Q_inv = classify_lines(app.ZerosOfB, Q_inv);
                end
                
                colors = [ 0.2, 0.7, 0.3; ...
                           0.5, 0.5, 0.5; ...
                           0.5, 0, 0.5; ...
                           0.5, 0.5, 0; ...
                           1, 0, 0];

                for k=1:size(Q_inv, 1)
                    Q_inv_h = refractiveIndBlasch(app, Q_inv(k,2:end-1), normalising_factor);
                    Q_inv_h(Q_inv_h > 2) = 2;
                    %Q_inv_h = Q_inv_h./max(max(Z_blasch));

                    plot3(app.UIAxes_2, ...
                        real(Q_inv(k, 2:end-1)), ...
                        imag(Q_inv(k, 2:end-1)), ...
                        Q_inv_h, ...
                        'color', colors(k,:), ...
                        'LineWidth', 4);
                end
            end
        end
        
        function StopButtonPushed(app, event)
            setState(app, STATE.IDLE, 'idle');
            
            % Abort excecution if needed
            app.Abort = true;
            
            % Clear application states
            app.Gamma = [];
            app.ZerosOfB = [];
            app.EndpointsOfG = [];
            
            % Clean the axes and re-draw the Toruses
            cla(app.UIAxes);
            cla(app.UIAxes_2);
            
            % Change axes view to 2D
            changeAxesView(app, 2);
            
            drawTorus2D(app);
        end
        
        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure and hide until all components are created
            app.UIFigure = uifigure('Visible', 'off');
            app.UIFigure.Position = [100 100 640 480];
            app.UIFigure.Name = 'Light paths demo';
            app.UIFigure.WindowButtonDownFcn = createCallbackFcn(app, @UIFigureWindowButtonDown, true);

            % Create CurrentstateLabel
            app.CurrentstateLabel = uilabel(app.UIFigure);
            app.CurrentstateLabel.Position = [34 446 82 22];
            app.CurrentstateLabel.Text = 'Current state: ';

            % Create IdleLabel
            app.StateLabel = uilabel(app.UIFigure);
            app.StateLabel.Position = [125 446 500 22];
            app.StateLabel.Text = 'Idle';

            % Create StateselectionLabel
            app.StateselectionLabel = uilabel(app.UIFigure);
            app.StateselectionLabel.Position = [34 415 92 22];
            app.StateselectionLabel.Text = 'State selection: ';

            % Create RegisterZerosButton
            app.RegisterZerosButton = uibutton(app.UIFigure, 'push');
            app.RegisterZerosButton.Position = [133 413 100 24];
            app.RegisterZerosButton.ButtonPushedFcn = ...
                createCallbackFcn(app, @RegisterZerosButtonPushed, true);
            app.RegisterZerosButton.Text = 'Register Zeros';

            % Create RegistercurveendpointsButton
            app.RegistercurveendpointsButton = uibutton(app.UIFigure, 'push');
            app.RegistercurveendpointsButton.Position = [245 413 152 24];
            app.RegistercurveendpointsButton.ButtonPushedFcn = ...
                createCallbackFcn(app, @RegistercurveendpointsButtonPushed, true);
            app.RegistercurveendpointsButton.Text = 'Register curve endpoints';

            % Create RunButton
            app.RunButton = uibutton(app.UIFigure, 'push');
            app.RunButton.Position = [406 413 100 24];
            app.RunButton.ButtonPushedFcn = ...
                createCallbackFcn(app, @RunButtonPushed, true);
            app.RunButton.Text = 'Run';

            % Create StopButton
            app.StopButton = uibutton(app.UIFigure, 'push');
            app.StopButton.Position = [519 413 100 24];
            app.StopButton.ButtonPushedFcn = ...
                createCallbackFcn(app, @StopButtonPushed, true);
            app.StopButton.Text = 'Stop';

            % Create CurvetypeButtonGroup
            app.CurvetypeButtonGroup = uibuttongroup(app.UIFigure);
            app.CurvetypeButtonGroup.Title = 'Curve type';
            app.CurvetypeButtonGroup.Position = [34 332 179 70];

            % Create EuclideanlinesegmentButton
            app.EuclideanlinesegmentButton = uiradiobutton(app.CurvetypeButtonGroup);
            app.EuclideanlinesegmentButton.Text = 'Perpendicular rays';
            app.EuclideanlinesegmentButton.Position = [11 24 152 22];
            app.EuclideanlinesegmentButton.Value = true;

            % Create HyperboliclinesegmentButton
            app.HyperboliclinesegmentButton = uiradiobutton(app.CurvetypeButtonGroup);
            app.HyperboliclinesegmentButton.Text = 'Abritary points';
            app.HyperboliclinesegmentButton.Position = [11 2 155 22];

            % Create UIAxes
            app.UIAxes = uiaxes(app.UIFigure);
            title(app.UIAxes, '\eta(z) := 1/(1-|z|^2)');
            app.UIAxes.Position = [1 1 315 308];
            axis(app.UIAxes, 'equal');
            hold(app.UIAxes, 'on');

            % Create UIAxes_2
            app.UIAxes_2 = uiaxes(app.UIFigure);
            title(app.UIAxes_2, '\eta(z) := |dB(z)|/|1-B(z)|^2');
            app.UIAxes_2.Position = [315 1 315 308];
            axis(app.UIAxes_2, 'equal');
            hold(app.UIAxes_2, 'on');
            
            % Show the figure after all components are created
            app.UIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = light_paths
            % Add needed libraries
            addpath('../Common');
            
            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)
            
            % Set current state as Idle
            setState(app, STATE.IDLE, 'Idle');
            
            % DrawTorus for point selection
            drawTorus2D(app);
            
            % Abort parameter set to false initially
            app.Abort = false;

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end