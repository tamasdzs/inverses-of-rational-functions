% Enumeration for current state
classdef STATE
    enumeration
        IDLE, REG_ZER, REG_CURVE, RUNNING
    end
end