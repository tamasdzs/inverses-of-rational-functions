% Invert monotonically increasing function that describes a
% Blaschke-product on the Torus
function inv_vals = Beta_inv(a, u)
    max_steps = 200;
    
    t_low = -2*pi;
    t_high = 2*pi;
    
    u_low = Beta(a, t_low);
    u_high = Beta(a, t_high);
    
    if u_low <= u && u < u_high
        for k=1:max_steps
           t_curr = (t_low + t_high)/2;
           u_curr = Beta(a, t_curr);
           
           if u_curr > u
                t_high = t_curr;
           else
                t_low = t_curr;
           end
        end
        
        inv_vals = t_curr;
    else
        error('error in beta inv');
    end
end