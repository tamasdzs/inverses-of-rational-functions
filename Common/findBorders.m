% Approximate the borders between the regions of the inverse branches

function [l, cp_images] = findBorders(a, cp, n, normalising_factor)

  if nargin < 3
     n = 512;
     normalising_factor = 1;
  end
  
  b = blaschkep(a, 1/normalising_factor);
  cp_images = b.values(cp);
  
  l = zeros(length(cp_images), n);
  
  for k=1:length(cp_images)
    m = euc_line_fit([real(cp_images(k)), imag(cp_images(k))], [0, 0]);
    x = linspace(real(exp(1i*angle(cp_images(k)))), real(cp_images(k)), n);
    x = sort(x);
    l(k,:) = x + 1i.*(m(1).*x + m(2));
  end
end  