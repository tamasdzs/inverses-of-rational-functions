% Invert the function f at w starting from z_0 using Newton's method

function z_fin = invert_w_Newton(w, z_0, f, f_der, err_tol)
    max_it = 1000;   
    z_fin = zeros(length(z_0), 1);
    
    % For each inverse branch
    for k=1:length(z_0)
        
        if length(w) > 1
           ww = w(k); 
        else
           ww = w;
        end
        
        % Set v to 0 in the beginning
        v = 0 + 1i*0;
        v_prev = 0 + 1i*0;
        
        % iteration control
        curr_it = 0;
        curr_err = 1;
        
        % Newton's method
        while curr_it < max_it && curr_err > err_tol
            v_prev = v;
            curr_it = curr_it + 1;
            v = v - (f(z_0(k) + v) - ww)/f_der(z_0(k) + v); 
            curr_err = abs(v - v_prev);
        end
        
        z_fin(k) = z_0(k) + v;
    end
end