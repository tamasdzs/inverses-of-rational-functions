function pol = polCoeffs(a)
    a = conj(a);
    N = length(a);
    pol = zeros(1, N+1);
    p = zeros(1,N);
    
    for k=N:-1:1
       if k == N
          p(k) = (-1)^N*prod(a);
       else
          p(k) = (-1)^(k).*sum(prod(nchoosek(a, k), 2));
       end
    end
    
    pol(1:N) = fliplr(p);
    pol(end) = 1;
end

