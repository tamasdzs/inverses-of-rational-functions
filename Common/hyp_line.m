function z = hyp_line(a_0, a_1, n)
    lambda=((1-a_0*conj(a_1))/(abs(1-a_0*conj(a_1))))*sqrt((1-a_0*conj(a_0))/(1-a_1*conj(a_1)));
    z=linspace(-1,1,n);
    z=(z.*(a_0-lambda*a_1) - (a_0+lambda*a_1))./(z.*(1-lambda)-(1+lambda));
end

