% Render inverse curves to a region of the disk

function ordered_lines = classify_lines(a, res)
  ordered_lines = zeros(size(res));
  
  for k=1:length(a)
    row_min = min(abs(res-a(k)),[], 2);
    [~, ind] = min(row_min);
    ordered_lines(k, :) = res(ind, :);
    res(ind,:) = [];
  end
end  