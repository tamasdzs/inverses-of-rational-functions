% Find critical points of an n-factor Blaschke-product

function [crit_points, omega] = findCriticalPoints(a)
    
    %% any poles with >= 2 multiplicities are also critical points
    [c, ia, ic] = unique(a);
    roots_and_multiplicities = [conj(c'), accumarray(ic, 1)];
    crit_points = c(find(roots_and_multiplicities(:,2) > 1));
    
    %% Create the polynomial omega
    for k=1:length(a)
        m = roots_and_multiplicities(k, 2) * ...
            (1 - abs(roots_and_multiplicities(k, 1))^2);
        
        prod_pol = [1];
        for j=1:length(a)
            if j ~= k
               aa = roots_and_multiplicities(j, 1);
               prod_pol = conv(prod_pol, ...
                            [-conj(aa) 1 + abs(aa)^2 -aa]);
            end
        end
        
        if k == 1
            omega = m.*prod_pol;
        else
            omega = omega + m.*prod_pol;
        end
    end
    
    %% Find the roots of omega (the rest of the critical points)
    crit_points = [crit_points; roots(omega)];
end

