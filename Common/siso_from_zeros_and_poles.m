function y = siso_from_zeros_and_poles(u, a, b, flip)
% Solves the convolutional equation p * y = q * u.
% INPUT:
%   -- u: input series
%   -- a: zeros of the polynomial q (zeros of the transfer function)
%   -- b: zeros of the polynomial p (poles of the transfer function)
% RETURNS:
%   -- y: output series

%% Parse input parameters
if nargin < 4
    flip = true;
end

%% Generate series, fix parameters
N = length(u);
y = zeros(size(u));
rs = zeros(size(u));
a_cf = zeros(size(u)); 
b_cf = zeros(size(u)); 

if flip
    a_cf(1:length(poly(a))) = transpose(fliplr(poly(a)));
    b_cf(1:length(poly(b))) = transpose(fliplr(poly(b)));
else
    a_cf(1:length(poly(a))) = transpose(poly(a));
    b_cf(1:length(poly(b))) = transpose(poly(b));
end

%% Generate output recursievely
for k=1:N
    s = 0;
    for j=1:k
       s = s + a_cf(j)*u(k-j+1); 
    end
    rs(k) = s;
end

for k=1:N
    s = 0;
    for j=1:k-1
        s = s + b_cf(j)*y(k-j+1);
    end
    y(k) = (rs(k) - s)/b_cf(1);
end 

end

