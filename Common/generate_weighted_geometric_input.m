function [u, w_final] = generate_weighted_geometric_input(c, MaxVal, N)
% Generates the input series w_final * c^k <= MaxVal (k=0,..,N) in a numerically stable manner.
% INPUT:
%   -- c: qoutient of the geometric series
%   -- MaxVal: The maximum value of the series.
%   -- N: The number of elements in the series
% RETURNS:
%   -- u: generated series
%   -- w_final: final weight (very small)

%% Init parameters
w_final = 1;
u = zeros(N, 1);

%% Generate series
u(1) = 1;
for k=2:N
    u(k) = c*u(k-1);

    if u(k) > MaxVal
        w = MaxVal/(u(k));
        u = w*u;
        w_final = w_final*w;
    end
end

end

