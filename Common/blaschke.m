% BLASCHKE - This class implements the Blaschke functions.

classdef blaschke
    properties
        parameter = 0;
        coefficient = 1;
    end
    
    methods
        
        % Constructing a Blaschke function object ---------------------
        
        function obj = blaschke(a,d)
            if nargin == 0
                a = 0;
                d = 1;
            end
            if nargin == 1
                d = 1;
            end
            if any(size(a) ~= [1 1])
                error('Parameter is not a single value!');
            end
            if abs(a) >= 1
                error('Parameter is not inside the disk!');
            end
            if abs(d) - 1 > eps
                error('Coefficient not unitary!');
            end
            obj.parameter = a;
            obj.coefficient = d;
        end
        
        
        % Calculation of function values ------------------------------
        
        function w = values(obj,z)
            w = obj.coefficient .* ...
                (z-obj.parameter) ./ (1 - conj(obj.parameter).*z);
        end
        
        function h = handle(obj)
            h = @(z)(obj.values(z));
        end
        
        % Plotting routines -------------------------------------------
        
        function plotd(obj, varargin)
            plotd(obj.handle,varargin{:});
        end
        
        function plott(obj,varargin)
            plott(obj.handle,varargin{:});
        end
    end
end
