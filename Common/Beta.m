function vals = Beta(a, t)
%Beta helps calculate the blaschke product values on the torus
%by calculating the Beta increasing function at t

n = length(a);
vals = zeros(size(t));

for k=1:n
    r = abs(a(k));
    alpha = angle(a(k));
    vals = vals + (Beta_r(t-alpha, r) + Beta_r(alpha, r));
end

vals = vals/n;

end