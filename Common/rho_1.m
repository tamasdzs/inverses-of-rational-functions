function dist = rho_1(z_1,z_2)
%Hyperbolic distance between two points
rho_0 = abs(z_1 - z_2)./abs(1 - conj(z_2).*z_1);
dist = 0.5*log((1+rho_0)./(1-rho_0));
end