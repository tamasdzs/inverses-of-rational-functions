% BLASCHKEP - This class implements the Blaschke products.

classdef blaschkep
    properties
        parameters = 0;
        coefficient = 1;
    end
    
    methods
        
        % Constructing a Blaschke product object ----------------------

        function obj = blaschkep(as,ds)
            if nargin == 0
                as = 0;
                ds = 1;
            end
            if nargin == 1
                ds = 1;
            end
            if size(as,1) > 1
                error('Not a row vector of parameters!');
            end
            if isa(as,'double')
                if size(ds,1) > 1
                    error('Not a row vector of coefficients!');
                end
                if size(ds,2) ~= 1 && size(ds,2) ~= size(as,2)
                    error('Wrong number of coefficients!');
                end
                % if any(abs(ds) ~= 1)
                %     error('Coefficient(s) not unitary!');
                % end
                ds = ds ./ abs(ds);
                obj.parameters = as;
                obj.coefficient = prod(ds);
            elseif isa(as,'blaschkep')
                if nargin ~= 1
                    error('Superfluous second argument!');
                end
                % number of factors
                nf = 0;
                for i = 1:length(as)
                    nf = nf + length(as(i).parameters);
                end
                % parameters = union of all parameters
                obj.parameters = zeros(1,nf);
                next = 1;
                for i = 1:length(as)
                    nfn = length(as(i).parameters);
                    obj.parameters(next:next+nfn-1) = as(i).parameters;
                    next = next + nfn;
                end
                % coefficient = product of all coefficients
                obj.coefficient = 1;
                for i = 1:length(as)
                    obj.coefficient = obj.coefficient * as(i).coefficient;
                end
            else
                error('Wrong type of parameters!');
            end
        end
        
        
        % Calculation of function values ------------------------------

        function w = values(obj,z)
            w = ones(size(z)) * obj.coefficient;
            for k = 1:length(obj.parameters)
                w = w .* blaschke(obj.parameters(k)).values(z);
            end
        end
        
        function bph = handle(obj)
            bph = @(z)(obj.values(z));
        end
        
        
        % Plotting routines -------------------------------------------

        function plotd(obj, varargin)
            plotd(obj.handle,varargin{:});
        end
        
        function plott(obj,varargin)
            plott(obj.handle,varargin{:});
        end
    end
end
