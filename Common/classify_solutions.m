function classified_sols = classify_solutions(prev, res_non_ordered)
% This function renders each solution to a region of the disk using the
% boundaries between the regions.

permutations = perms(res_non_ordered);
prev = repmat(prev, 1, size(permutations, 1));
res = transpose(permutations);
dist_mat = rho_1(prev, res);
[~, ind] = min(sum(dist_mat, 1)); 
classified_sols = res(:,ind);

end

