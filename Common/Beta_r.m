function b_r_vals = Beta_r(t, r)
    k = zeros(size(t));
    s = (1+r)/(1-r);

    for m=1:length(t)
       if t(m) < -pi 
           k(m) = ceil(t(m)/(2*pi));
           
           if t(m) - k(m)*2*pi < -pi
               k(m) = k(m)-1;
           end
           
           t(m) = t(m) - k(m)*2*pi;
       elseif t(m) > pi
           k(m) = floor(t(m)/(2*pi));
           
           if t(m) - k(m)*2*pi > pi
               k(m) = k(m)+1;
           end
           
           t(m) = t(m) - k(m)*2*pi;
       else
            % Left empty intentionally
       end 
    end
    b_r_vals = k*2*pi + 2*atan(s*tan(t/2));
end