% Find inverses of an n-factor Blaschke-product along the points of
% line_vals, using the built-in polynomial root finding methods

function res = findInvImages(a, line_vals, norming_coeff)

    num = poly(a);
    denom = polCoeffs(a);
    
    if nargin > 2
        denom = denom*norming_coeff;
    else
        norming_coeff = 1;
    end
    
    res = zeros(length(a), length(line_vals));

    for k=1:length(line_vals)
      res_non_ordered = roots(num - line_vals(k)*denom);
      
      if k > 1
          res(:,k) = classify_solutions(res(:, k-1), res_non_ordered);
      else
          res(:,k) = res_non_ordered;
      end
    end
end