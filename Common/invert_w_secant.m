% Find the inverse of f at w starting from the points defined in z_0 using
% the secant method

function z_fin = inv_no_der_general(w, z_0, f, err_tol)

    max_it = 10000;   
    z_fin = zeros(length(z_0), 1);
    
    % For each inverse branch
    for k=1:length(z_0)
        
        if length(w) > 1
           ww = w(k); 
        else
           ww = w;
        end
        
        % Set v to 0 in the beginning
        v = 0.01 + 1i*0.01;
        v_prev = 0 + 1i*0;
        
        % iteration control
        curr_it = 0;
        curr_err = 1;
        
        % We want to find the inverse value at w
        while curr_it < max_it && curr_err > err_tol   
            d = (f(z_0(k)+v) - f(z_0(k)));
            v_next = v - v*(f(z_0(k)+v)-ww)/d;
            
            v_prev = v;
            v = v_next;
            
            curr_it = curr_it + 1;
            curr_err = abs(v - v_prev);
        end
        
        z_fin(k) = z_0(k)+v;
    end
end