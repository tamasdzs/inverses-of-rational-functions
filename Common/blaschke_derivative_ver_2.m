% Calculate the first derivative of an n-factor Blaschke-product

function [dB_vals, B, denom, d_omega_1, d_omega_2] = blaschke_derivative_ver_2(a, z, e)
    
    if nargin < 3
       e = 1; 
    end
    
    B = blaschkep(a, 1/e);
    nom = poly(a);
    denom = polCoeffs(a)*e;
    
    [p, q] = polyder(nom, denom);
    dB_vals = polyval(p, z)./ polyval(q, z);
    d_omega_1 = polyder(nom);
    d_omega_2 = polyder(denom);
end

