function b = blaschke_vals(a, z)
% classic definition of blaschke products
b = prod((z-a)./(1-conj(a).*z));
end