% Fit a line segment on two points

function c = euc_line_fit(p_0, p_1)
  A = [ p_0(1) 1; p_1(1) 1 ];
  b = [ p_0(2); p_1(2) ];
  c = A\b;
end  