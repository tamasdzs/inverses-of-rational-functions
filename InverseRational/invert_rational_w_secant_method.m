% Find inverse branches of a rational function using the developed
% algorithm, locally employing secant method

% Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
% Technology from the source of the National Research, Development and Innovation Fund.

addpath('../Common');

%% Clear everything just to be sure
clear all;
close all;

%% Constants

% roots of f
a = [-0.51 - 1i*0.15, ...
      0.25 + 1i*0.73];
  
% Poles of f
b = [-1.32 + 1i*1.4; ...
    2.1 - 1i*0.2; ...
    1.3 + 1i*2.3];

% Torus points
fi = linspace(-pi, pi, 1024);
T  = exp(1i*fi);

% Callback for f
f = @(z) polyval(poly(a), z)./polyval(poly(b), z);

% Nyquist plot
R = f(T);

%% Find a single inverse on R
rng default;
u_1_ind = randi([1, length(R)]);
u_1 = R(u_1_ind);

% Get inv images on the Torus
angles = angle(R);

[u_1_inv_err, u_1_inv_ind] = min(abs(angles-angle(u_1)));
u_1_inv = exp(1i*fi(u_1_inv_ind));

angles(u_1_inv_ind) = Inf;
[u_2_inv_err, u_2_inv_ind] = min(abs(angles-angle(u_1)));
u_2_inv = exp(1i*fi(u_2_inv_ind));
u_2 = R(u_2_inv_ind);

%% Plot range and domain spaces along with crit_points & images
subplot(121); hold on;
plot(R, 'k', 'LineWidth', 2);
plot(0,0, 'ro', 'MarkerSize', 8, 'MarkerFaceColor', 'r');
plot(real(u_1), imag(u_1), 'bo', 'MarkerSize', 8, 'MarkerFaceColor', 'b');
plot(real(u_2), imag(u_2), 'bo', 'MarkerSize', 8, 'MarkerFaceColor', 'b');
title('$f(D)$', 'interpreter', 'latex');
grid on;
axis equal;

subplot(122); hold on;
plot(T, 'k', 'LineWidth', 2);
plot(a, 'ro', 'MarkerSize', 8, 'MarkerFaceColor', 'r');
plot(b, 'mo', 'MarkerSize', 8, 'MarkerFaceColor', 'm');
plot(real(u_1_inv), imag(u_1_inv), 'bo', 'MarkerSize', 8, 'MarkerFaceColor', 'b');
plot(real(u_2_inv), imag(u_2_inv), 'bo', 'MarkerSize', 8, 'MarkerFaceColor', 'b');
title('$D$', 'interpreter', 'latex');
grid on;
axis equal;

%% Do the iteration to find all roots

step = 0.001;
rho_1 = abs(u_1)-step;
rho_2 = abs(u_2)-step;
phi_1 = angle(u_1);
phi_2 = angle(u_2);

rho = [rho_1, rho_2];
phi = [phi_1, phi_2];

z_0 = transpose([u_1_inv, u_2_inv]);

% Holder for verification values
ws = [];
w = transpose([u_1, u_2]);

iter = 0;
max_iter = 100000;

not_done = 1;

% Solve task with old line-based method
while not_done > 0 && iter < max_iter
   iter = iter+1;
   
   not_done = 0;
   
   for k=1:length(w)
       if abs(w(k)) > 0.001
        w(k) = rho(k)*exp(1i*phi(k));
        rho(k) = rho(k)-step;
        not_done = not_done + 1/length(w);
       end
   end
   
   z_0 = invert_w_secant(w, z_0, f, 2*eps);
   
   for k=1:length(w)
       subplot(1, 2, 1);
       plot(w(k), 'go','MarkerSize', 6, 'MarkerFaceColor', 'g'); hold on;
   end
   subplot(1, 2, 2);
   plot(z_0, 'go', 'MarkerSize', 6, 'MarkerFaceColor', 'g'); hold on;
   pause(0.001);
   drawnow;
end

