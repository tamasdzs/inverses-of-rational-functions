% Find inverse branches of a rational function using the developed
% algorithm, locally employing Newton's method

% Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
% Technology from the source of the National Research, Development and Innovation Fund.

addpath('../Common');

%% Clear everything just to be sure
clear all;
close all;

%% Constants

% roots of the rational function (in D)
a = [-0.51 - 1i*0.15, ...
      0.25 + 1i*0.73];
  
% Poles of the rational function
b = [1.3 + 1i*0.13, ...
    -1.1 - 1i*1.0];

% Torus points
fi = linspace(-pi, pi, 1024);
T  = exp(1i*fi);

% Define the polynomial & its derivative
f = @(z) polyval(poly(a), z)./polyval(poly(b), z);
f_der = @(z) (polyval(polyder(poly(a)), z).*polyval(poly(b), z) - polyval(poly(a), z).*polyval(polyder(poly(b)), z))./ ...
    (polyval(poly(b), z)).^2;

% Get mapping of T
R = f(T);

% Get critical points, and images of critical points
crit_points = roots(polyder(poly(a)));
crit_points_images = f(crit_points);

%% Find a single inverse on R
u_1_ind = 500;

u_1 = R(u_1_ind);

% Get inv images on edge
angles = angle(R);

[u_1_inv_err, u_1_inv_ind] = min(abs(angles-angle(u_1)));
u_1_inv = exp(1i*fi(u_1_inv_ind));

angles(u_1_inv_ind) = Inf;
[u_2_inv_err, u_2_inv_ind] = min(abs(angles-angle(u_1)));
u_2_inv = exp(1i*fi(u_2_inv_ind));
u_2 = R(u_2_inv_ind);

%% Plot T, Nyquist-plot, zeros, poles, k, K
h = figure;
subplot(121); hold on;
plot(T, 'k', 'LineWidth', 3);
plot(a, 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
plot(b, 'mo', 'MarkerSize', 10, 'MarkerFaceColor', 'm');
plot(crit_points, 'co', 'MarkerSize', 10, 'MarkerFaceColor', 'c');
title('$D$', 'interpreter', 'latex');
axis equal;

subplot(122); hold on;
plot(R, 'k', 'LineWidth', 3);
plot(0,0, 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r');
plot(real(crit_points_images), imag(crit_points_images), 'co', ...
    'MarkerSize', 10, 'MarkerFaceColor', 'c');
title('$f(D)$', 'interpreter', 'latex');
axis equal;

%% Plot w_0 on the Nyquist-plot
subplot(122); hold on;
plot(u_1, 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b');
plot(u_2, 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b');

%% Plot the inverses of w_0
subplot(121); hold on;
plot(u_1_inv, 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b');
plot(u_2_inv, 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b');

%% Do the iteration to find all roots
step = 0.01;
rho_1 = abs(u_1) - step;
rho_2 = abs(u_2) - step;
phi_1 = angle(u_1);
phi_2 = angle(u_2);
z_0 = transpose([u_1_inv, u_2_inv]);
z_0_old = z_0;

% Holder for verification values
ws = [];

w = [u_1, u_2];

iter = 0;
max_iter = 1000;

not_done = 1;

% Solve task with old line-based method
while not_done > 0 && iter < max_iter
   iter = iter+1;
   
   not_done = 0;
   
   if abs(w(1)) > 0.01
    w(1) = rho_1*exp(1i*phi_1);
    rho_1 = rho_1-step;
    not_done = not_done + 0.5;
   end
   
   if abs(w(2)) > 0.01
    w(2) = rho_2*exp(1i*phi_2);
    rho_2 = rho_2-step;
    not_done = not_done + 0.5;
   end
   
   z_0_old = [z_0_old, invert_w_Newton(w, z_0_old(:,end), f, f_der, eps)];
   
   subplot(1, 2, 2);
   plot(w(1), 'go','MarkerSize', 8, 'MarkerFaceColor', 'g'); hold on;
   plot(w(2), 'go','MarkerSize', 8, 'MarkerFaceColor', 'g'); hold on;
   subplot(1, 2, 1);
   plot(z_0_old(:, end), 'go', 'MarkerSize', 6, 'MarkerFaceColor', 'g'); hold on;
   pause(0.001);
   drawnow;
end