% Calculate the roots of orthogonal Legendre polynomials. The algorithm
% produces the Legendre polynomial in question according to the three-term
% recursion formula. Using a modified version of this formula we can also
% produce the reciprocate polynomials. The ration of these two yields a
% Blaschke-product, thus our inverting algorithms can be applied to find
% the roots of the original polynomial.

% Supported by the ÚNKP-21-3 New National Excellence Program of the Ministry for Innovation and
% Technology from the source of the National Research, Development and Innovation Fund.

clear all;
close all;

addpath('../Common');

%% Constants
N = 1024;
M = 2;
STEPS = ['a'; 'b'];% ['a'; 'b'; 'c'];

% M = 2
GT = [0.5774   -0.5774]';

% M = 3
%GT = [0; 0.7746; -0.7746];

% M = 4
%GT = [-0.8611; 0.8611; -0.3400; 0.3400];

%GT = roots([1 0 -6 0 3]);

% M = 5
%GT = [0   -0.9062   -0.5385    0.9062    0.5385]';

% M = 6
%GT = [-0.9325   -0.6612    0.9325    0.6612   -0.2386    0.2386]';

% M >6
%GT = [];

%% STEPS
if any(STEPS == 'a')
    z = linspace(-2, 2, N)';
    h = z(2)-z(1);
    [L, ab] = LegendreMonic(z, M);

    R = constructRecPol(ab, z);

    for k=1:size(L,2)
        figure(1);
        cla;
        hold on;
        plot(z, L(:,k), 'b', 'LineWidth', 3);
        plot(z, R(:,k), 'r', 'LineWidth', 3);
        grid on;
        axis tight;
        legend('Legendre polynomial', 'Reciprocate Legendre Polynomial');
        pause();
    end
end

if any(STEPS == 'b')
    % Check that it really is a Blaschke-product -> should map T onto itself
    % N-fold
    fi = linspace(-pi, pi, N);
    T = transpose(exp(1i*fi));
    
    real_line = linspace(-1, 1, N);
    real_line = real_line + 1i*0.0;
    
    f = @(z) getBlaschkeVal(z, M);
    
    % Get inverse on Torus
    R = f(T);
    %rng default;
    u_1_ind = randi([1, length(R)]);
    u_1 = R(u_1_ind);
    angles = angle(R);
    u_invs = zeros(M, 1);
    prev_inds = [];
    for k=1:M
        found = false;
        while ~found
            [~, inv_ind] = min(abs(angles-angle(u_1)));
            u_invs(k) = exp(1i*fi(inv_ind));
            angles(inv_ind) = Inf;
            
            if isempty(prev_inds) || min(abs(prev_inds - inv_ind)) > 10
               found = true; 
               prev_inds = [prev_inds, inv_ind];
            end
        end
    end
    
    % Plot results on the Torus
    figure();
    subplot(122); hold on;
    plot(real(R), imag(R), 'k', 'LineWidth', 4);
    plot(u_1, 'bo', 'MarkerSize', 20, 'MarkerFaceColor', 'b');
    plot(real(real_line), imag(real_line), 'k--', 'LineWidth', 2);
    plot(0, 0, 'ro', 'MarkerSize', 20, 'MarkerFaceColor', 'r');
    axis square;
    grid on;
    title('B(D)');
    
    subplot(121); hold on;
    plot(T, 'k', 'LineWidth', 4);
    plot(u_invs, 'bo', 'MarkerSize', 20, 'MarkerFaceColor', 'b');
    plot(real(real_line), imag(real_line), 'k--', 'LineWidth', 1.5);
    
    % if ground truth is available
    if ~isempty(GT)
       plot(real(GT), imag(GT), 'ro', 'MarkerSize', 20, 'MarkerFaceColor', 'r'); 
    end
    
    axis square;
    grid on;
    title('D');
    
    % Run the iterative algorithm to find the roots
    step = 0.001;
    w_curr = u_1;
    w_invs = u_invs;
    ang = angle(u_1);
    
    while abs(w_curr) > step
        curr_rho = abs(w_curr);
        w_curr = (curr_rho-step)*exp(1i*ang);
        w_invs = invert_w_secant(w_curr, w_invs, f, eps);
        
        subplot(122);
        plot(w_curr, 'go', 'MarkerSize', 8, 'MarkerFaceColor', 'g');
        
        subplot(121);
        plot(w_invs, 'go', 'MarkerSize', 8, 'MarkerFaceColor', 'g');
        
        drawnow;
        
        if abs(w_curr - u_1) <= (step + eps)
            pause();
        end
    end
        
end

%% Routines
function [L,ab] = LegendreMonic(z, m)
% z: point of evaluation
% m: degree (up to)

% Determine the recurrence coeffs
ab = zeros(m, 2);
for k=1:size(ab, 1)
   if k == 1
      ab(k,2) = 2; 
   else
      ab(k,2) = 1/(4 - (k-1)^(-2));
   end
end

% Using the recurrence coeffs find the Legendre system
L = constructOrthPol(ab, z);

end

function L = constructOrthPol(ab, z)
    N = size(ab, 1);
    L = (z - ab(1, 1)).*ones(length(z), 1) - ab(1,2).*zeros(length(z), 1);
    L = [L, (z - ab(2, 1)).*L - ab(2,2).*ones(length(z), 1)];
    for k=3:N
        L = [L, (z - ab(k, 1)).*L(:,k-1) - ab(k,2)*L(:,k-2)];
    end
end

function L = constructRecPol(ab, z)
    N = size(ab, 1);
    L = (1 - ab(1, 1)*z).*ones(length(z), 1) - ab(1,2).*(z.^2).*zeros(length(z), 1);
    L = [L, (1 - ab(2, 1)*z).*L - ab(2,2).*(z.^2).*ones(length(z), 1)];
    for k=3:N
        L = [L, (1 - ab(k, 1)*z).*L(:,k-1) - ab(k,2).*(z.^2).*L(:,k-2)];
    end
end

function z_ret = getBlaschkeVal(z, m)
    [L, ab] = LegendreMonic(z, m);
    R = constructRecPol(ab, z);
    L = L(:,end);
    R = R(:,end);
    
    % Norm it
    R_one = constructOrthPol(ab, 1);
    L_one = constructRecPol(ab, 1);
    norm_term = L_one/R_one;
    
    z_ret = (L./R)*norm_term;
end